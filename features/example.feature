Feature: Example feature

  Some good feature description.

  Background:
    Given All is ready

  Scenario: Calculator
    When I add 2 and 2
    Then I should get 4

  Scenario: Google Search
    When I open Google
    And Search for something
    Then I should get the result

  Scenario Outline: Eating cucumbers
    Given there are <start> cucumbers
    When I eat <eat> cucumbers
    Then I should have <left> cucumbers

    Examples:
      | start | eat | left |
      |  12   |  5  |  7   |
      |  20   |  5  |  15  |