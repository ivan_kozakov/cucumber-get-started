Given(/^All is ready$/) do
  true
end

When(/^I add (\d+) and (\d+)$/) do |first, second|
  @result = first.to_i + second.to_i
end

Then(/^I should get (\d+)$/) do |expected_result|
  expect(expected_result.to_i).to eq(@result)
end

When(/^I open Google$/) do
  visit ''
end

When(/^Search for something$/) do
  input = find '#lst-ib'
  input.set('Cisco')
  find('#sblsbb').click
end

Then(/^I should get the result$/) do
  sleep 2
  expect(page.text).to include('is the worldwide leader')
end

Given(/^there are (\d+) cucumbers$/) do |start|
  @start = start.to_i
end

When(/^I eat (\d+) cucumbers$/) do |eat_ones|
  @eat_ones = eat_ones.to_i
end

Then(/^I should have (\d+) cucumbers$/) do |left|
  expect(left.to_i).to eq(@start-@eat_ones)
end

