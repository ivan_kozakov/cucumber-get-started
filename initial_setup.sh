#!/usr/bin/env bash

# Install RVM
\curl -sSL https://get.rvm.io | bash
source ~/.rvm/scripts/rvm
# You also can install some specific ruby version by typing 

rvm install 2.0.0-p645

# This is needed only if you are setting up the env... otherwise the files will be created already.
# Set up the app using specific Ruby version and specific gemset
#echo "cucumber-get-started" > .ruby-gemset
#echo "ruby-2.0.0-p645" > .ruby-version

cd .. && cd -

bundle install
